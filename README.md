# Tic-tac-toe

README / 07 June 2019

-----

## Introduction

Tic-tac-toe is an implementation of a popular Tic-tac-toe game. Written entirely in C# using WPF. Only singleplayer mode supported. Human player always starts the turn. For AI the minimax algorithm was used but without alpha-beta pruning.

![Game preview](https://gitlab.com/Prologh/tic-tac-toe/raw/master/docs/preview.png "Game preview")

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | .NET Framework 4.7

## Getting started

Download the [1.0 version](https://gitlab.com/Prologh/tic-tac-toe/tags/v1.0) from GitLab or clone the project and build it on your own.

## Building prerequisites

To build this project you'll need:

 - Microsoft Visual Studio 2017 (or later)
 - Microsoft .NET Framework 4.7 (usually comes with Visual Studio)

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/tic-tac-toe/blob/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).