﻿namespace TicTacToe.DesktopApp.Models.Statuses
{
    /// <summary>
    /// Represents ongining game status.
    /// </summary>
    public class OngoingStatus : GameStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OngoingStatus"/> class.
        /// </summary>
        public OngoingStatus() : base(GameEndingType.StillOngoing)
        {

        }
    }
}
