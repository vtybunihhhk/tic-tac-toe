﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Models.Statuses
{
    /// <summary>
    /// Represents the game status.
    /// </summary>
    [DebuggerDisplay("EndingType = {EndingType}")]
    public abstract class GameStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameStatus"/> class.
        /// </summary>
        /// <param name="gameEndingType">
        /// The type of game ending.
        /// </param>
        protected GameStatus(GameEndingType gameEndingType)
        {
            EndingType = gameEndingType;
            WinningFields = new List<FieldVM>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameStatus"/> class.
        /// </summary>
        /// <param name="gameEndingType">
        /// The type of game ending.
        /// </param>
        /// <param name="winningFields">
        /// A collection of fields being a direct cause of game end.
        /// </param>
        /// <param name="winningPlayer">
        /// The player who won the game.
        /// </param>
        protected GameStatus(GameEndingType gameEndingType, IEnumerable<FieldVM> winningFields, Player winningPlayer)
        {
            EndingType = gameEndingType;
            WinningFields = winningFields.ToList() ?? throw new ArgumentNullException(nameof(winningFields));
            Winner = winningPlayer ?? throw new ArgumentNullException(nameof(winningPlayer));
        }

        /// <summary>
        /// Gets the game ending type.
        /// </summary>
        public GameEndingType EndingType { get; }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current game is over.
        /// </summary>
        public bool IsGameOver => EndingType != GameEndingType.StillOngoing;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current game is
        /// still ongoing.
        /// </summary>
        public bool IsOngoing => EndingType == GameEndingType.StillOngoing;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current game
        /// has ended with a draw.
        /// </summary>
        public bool IsTie => EndingType == GameEndingType.Tie;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current game
        /// has ended with victory for one of players.
        /// </summary>
        public bool IsVictory => EndingType == GameEndingType.Victory;

        /// <summary>
        /// Gets the list of marked fields being a direct cause of victory.
        /// </summary>
        public IReadOnlyList<FieldVM> WinningFields { get; }

        /// <summary>
        /// Gets the player who won the game.
        /// </summary>
        public Player Winner { get; }
    }
}
