﻿namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Indicates game ending type.
    /// </summary>
    public enum GameEndingType
    {
        StillOngoing = 0,
        Tie,
        Victory
    }
}
