﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using TicTacToe.DesktopApp.Services;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents game board.
    /// </summary>
    [DebuggerDisplay("[{Columns} x {Rows}] ({Fields.Count})")]
    public class Board
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/> class.
        /// </summary>
        /// <param name="gameManager">
        /// The game manager owning this board instance.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="gameManager"/> is <see langword="null"/>.
        /// </exception>
        public Board(IGameManager gameManager)
        {
            GameManager = gameManager ?? throw new ArgumentNullException(nameof(gameManager));

            Positions = typeof(Position)
                .GetProperties(BindingFlags.Public | BindingFlags.Static)
                .Select(p => p.GetValue(null))
                .Cast<Position>()
                .Where(p => !p.IsUnset)
                .ToList();

            Fields = new ReadOnlyObservableCollection<FieldVM>(
                new ObservableCollection<FieldVM>(
                    Positions.Select(position => new FieldVM(this, position))
                )
            );
        }

        /// <summary>
        /// Gets the amount of columns.
        /// </summary>
        public int Columns => (int)Math.Sqrt(Positions.Count);

        /// <summary>
        /// Gets the collection of fields.
        /// </summary>
        public ReadOnlyObservableCollection<FieldVM> Fields { get; }

        /// <summary>
        /// Gets the game manager owning this board instance.
        /// </summary>
        public IGameManager GameManager { get; }

        /// <summary>
        /// Gets the list of positions.
        /// <para/>
        /// This list does not include <see cref="Position.Unset"/>.
        /// </summary>
        public IReadOnlyList<Position> Positions { get; }

        /// <summary>
        /// Gets the amount of rows.
        /// </summary>
        public int Rows => (int)Math.Sqrt(Positions.Count);
    }
}
