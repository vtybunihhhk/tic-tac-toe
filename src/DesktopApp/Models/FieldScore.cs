﻿namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents field score and position.
    /// </summary>
    public struct FieldScore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/> class
        /// with specified field score.
        /// </summary>
        /// <param name="score">
        /// The field score.
        /// </param>
        public FieldScore(int score)
        {
            Position = default(Position);
            Score = score;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldScore"/> class
        /// with specified field position.
        /// </summary>
        /// <param name="position">
        /// The field position.
        /// </param>
        public FieldScore(Position position)
        {
            Position = position;
            Score = default(int);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldScore"/> class
        /// with specified field position and score.
        /// </summary>
        /// <param name="index">
        /// The field position.
        /// </param>
        /// <param name="score">
        /// The field score.
        /// </param>
        public FieldScore(Position position, int score)
        {
            Position = position;
            Score = score;
        }

        /// <summary>
        /// Gets or sets the field position.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// Gets or sets the field score.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current field score.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current field score.
        /// </returns>
        public override string ToString()
        {
            return $"{(Position?.ToString() ?? string.Empty)} : {Score}";
        }
    }
}
