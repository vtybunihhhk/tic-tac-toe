﻿using System;
using System.Windows;
using System.Windows.Controls;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Views
{
    /// <summary>
    /// Interaction logic for FieldControl.xaml
    /// </summary>
    public partial class FieldControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldVM"/> class
        /// with specified parent context and field position.
        /// </summary>
        public FieldControl()
        {
            InitializeComponent();
        }

        private async void FieldButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button))
            {
                throw new ArgumentException(
                    $"Sender must be an instance of {typeof(Button).FullName}.",
                    nameof(sender));
            }

            if (!(DataContext is FieldVM field))
            {
                throw new InvalidOperationException(
                    $"Provided data context for this {typeof(FieldControl).Name} " +
                    $"must be an instance of {typeof(FieldVM).FullName}.");
            }

            await field.GameManager.PerformPlayerMoveAsync(field);
        }
    }
}
