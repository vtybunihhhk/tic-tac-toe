﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.Services;

namespace TicTacToe.DesktopApp.ViewModels
{
    /// <summary>
    /// Represents field model.
    /// </summary>
    [DebuggerDisplay("{Position}: Mark = {Mark}")]
    public class FieldVM : INotifyPropertyChanged
    {
        private object _content;
        private Brush _foreground;
        private bool _isEnabled;
        private Mark _mark;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldVM"/> class
        /// with specified parent context and field position.
        /// </summary>
        /// <param name="board">
        /// The game board.
        /// </param>
        /// <param name="position">
        /// The field position.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="board"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="position"/> is <see langword="null"/>.
        /// </exception>
        public FieldVM(Board board, Position position)
        {
            Board = board ?? throw new ArgumentNullException(nameof(board));
            Position = position ?? throw new ArgumentNullException(nameof(position));

            _foreground = new SolidColorBrush(Colors.White);
            _isEnabled = true;
            _mark = Mark.Empty;
        }

        /// <summary>
        /// Gets the game board.
        /// </summary>
        public Board Board { get; }

        /// <summary>
        /// Gets or sets field content.
        /// </summary>
        public object Content
        {
            get { return _content; }

            set
            {
                if (Content != value)
                {
                    _content = value;
                    RaisePropertyChanged(nameof(Content));
                }
            }
        }

        /// <summary>
        /// Gets or sets field foreground brush.
        /// </summary>
        public Brush Foreground
        {
            get { return _foreground; }

            set
            {
                if (Foreground != value)
                {
                    _foreground = value;
                    RaisePropertyChanged(nameof(Foreground));
                }
            }
        }

        /// <summary>
        /// Gets the game manager owning the board associated with this field
        /// instance.
        /// </summary>
        public IGameManager GameManager => Board.GameManager;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether field mark
        /// is set to <see cref="Mark.Empty"/>.
        /// </summary>
        public bool HasEmptyMark => Mark.IsEmpty;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether field position
        /// is set to <see cref="Position.Unset"/>.
        /// </summary>
        public bool HasUnsetPosition => Position.IsUnset;

        /// <summary>
        /// Gets or sets a <see cref="bool"/> value indictating whether
        /// related to the current field control should be enabled or not.
        /// </summary>
        public bool IsEnabled
        {
            get { return _isEnabled; }

            set
            {
                if (IsEnabled != value)
                {
                    _isEnabled = value;
                    RaisePropertyChanged(nameof(IsEnabled));
                }
            }
        }

        /// <summary>
        /// Gets or sets field mark.
        /// </summary>
        public Mark Mark
        {
            get { return _mark; }

            set
            {
                if (Mark != value)
                {
                    _mark = value;
                    RaisePropertyChanged(nameof(Mark));
                }
            }
        }

        /// <summary>
        /// Gets the field position.
        /// </summary>
        public Position Position { get; }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Invokes <see cref="PropertyChangedEventHandler"/> for specified
        /// property.
        /// </summary>
        /// <param name="propertyName">
        /// The name of changed property.
        /// </param>
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
