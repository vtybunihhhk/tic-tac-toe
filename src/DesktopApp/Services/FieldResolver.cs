﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.Models.Statuses;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Provides algorithmic field resolving functionalities.
    /// </summary>
    public class FieldResolver : IFieldResolver
    {
        private readonly IGameStatusEvaluator _evaluator;
        private readonly IGameStatusResolver _resolver;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldResolver"/> class.
        /// </summary>
        /// <param name="context">
        /// The field context.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="context"/> is <see langword="null"/>.
        /// </exception>
        public FieldResolver(IPlayerManager playerManager)
        {
            _evaluator = new GameStatusEvaluator();
            _playerManager = playerManager;
            _resolver = new GameStatusResolver(playerManager);
        }

        /// <summary>
        /// Asynchronously finds next field that shoule be marked.
        /// </summary>
        /// <param name="fields">
        /// The fields collection representing current state of board.
        /// </param>
        /// <param name="player">
        /// The player for which next field should be resolved.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Task{TResult}"/> representing operation.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="player"/> is <see langword="null"/>.
        /// </exception>
        public async Task<FieldVM> ResolveAsync(IReadOnlyList<FieldVM> fields, Player player)
        {
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            return await Task.Run(() =>
            {
                Thread.Sleep(500);

                var resolvedScore = MinMax(fields.ToDictionary(field => field.Position, field => field), player, depth: 1);

                return fields.First(field => field.Position == resolvedScore.Position);
            });
        }

        private FieldScore MinMax(IReadOnlyDictionary<Position, FieldVM> newBoard, Player player, int depth)
        {
            // Get the unmarked fields.
            IReadOnlyList<FieldVM> emptyFields = newBoard
                .Select(pair => pair.Value)
                .Where(field => field.HasEmptyMark)
                .ToList();

            // Check if current board is not indicating game's end.
            var gameStatus = _resolver.Resolve(newBoard.Select(pair => pair.Value));
            if (gameStatus.IsGameOver)
            {
                // Evaluate the game status.
                var gameStatusValue = _evaluator.EvaluateGameStatus(gameStatus, player);

                return new FieldScore(score: gameStatusValue);
            }

            // A list to collect all the potential moves.
            var moves = new List<FieldScore>();

            // Loop through empty fields.
            foreach (var currentField in emptyFields)
            {
                // Set the mark of empty field according to the mark used by current player.
                newBoard[currentField.Position].Mark = player.Mark;

                // Get the opposing player instance for passing it in the next MinMax call.
                var opponent = _playerManager.GetOpponent(player);

                // Create a field score and store the position of current field.
                // Recursion!
                var minMaxFieldScore = MinMax(newBoard, opponent, depth + 1);
                var result = new FieldScore(currentField.Position, minMaxFieldScore.Score);

                // Reset the field's mark to empty.
                newBoard[currentField.Position].Mark = Mark.Empty;

                // Add the field score to the list of moves.
                moves.Add(result);
            }

            return FindBestMove(moves, player, depth);
        }

        private FieldScore FindBestMove(IList<FieldScore> moves, Player player, int depth)
        {
            // If it is the computer's turn loop over the moves
            // and choose the move with the highest score.
            var bestMove = new FieldScore();
            if (player.IsComputer)
            {
                var bestScoreValue = int.MinValue;
                for (var index = 0; index < moves.Count; index++)
                {
                    if (moves[index].Score > bestScoreValue)
                    {
                        bestScoreValue = moves[index].Score;
                        bestMove = moves[index];
                    }
                }
            }
            else
            {
                // Else loop over the moves and choose the move with the lowest score.
                var bestScoreValue = int.MaxValue;
                for (var index = 0; index < moves.Count; index++)
                {
                    if (moves[index].Score < bestScoreValue)
                    {
                        bestScoreValue = moves[index].Score;
                        bestMove = moves[index];
                    }
                }
            }

            // If there are more then one best move - randomize one out of them.
            var bestMovesWithTheSameScore = moves.Where(fieldScore => fieldScore.Score == bestMove.Score).ToList();
            if (bestMovesWithTheSameScore.Count > 1)
            {
                var randomIndex = new Random().Next(bestMovesWithTheSameScore.Count);
                bestMove = bestMovesWithTheSameScore[randomIndex];
            }

            // Return the chosen move (field score) from the list to the higher depth.
            return bestMove;
        }
    }
}
