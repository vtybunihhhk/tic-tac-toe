﻿using System.Windows.Media;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Defines minimum requirements for mark helper.
    /// </summary>
    public interface IMarkHelper
    {
        /// <summary>
        /// Gets is enabled <see cref="bool"/> value corresponding to provided
        /// <see cref="Mark"/>.
        /// </summary>
        /// <param name="mark">
        /// The <see cref="Mark"/>.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> value.
        /// </returns>
        bool GetMarkIsEnabled(Mark mark);

        /// <summary>
        /// Gets foreground brush corresponding to provided <see cref="Mark"/>.
        /// </summary>
        /// <param name="mark">
        /// The <see cref="Mark"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Brush"/>.
        /// </returns>
        Brush GetMarkForeground(Mark mark);
    }
}
