﻿using System.Collections.Generic;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Defines minimum requirements for palyer managing service.
    /// </summary>
    public interface IPlayerManager
    {
        /// <summary>
        /// Gets the computer player.
        /// </summary>
        Player Computer { get; }

        /// <summary>
        /// Gets the human player.
        /// </summary>
        Player Human { get; }

        /// <summary>
        /// Gets the players collection.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Player> Players { get; }

        /// <summary>
        /// Returns the opposing player.
        /// </summary>
        /// <param name="player">
        /// The current player.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Player"/>.
        /// </returns>
        Player GetOpponent(Player player);
    }
}
