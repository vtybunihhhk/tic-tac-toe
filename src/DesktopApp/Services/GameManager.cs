﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.Models.Statuses;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Manages game, field context, grid fields and computer moves.
    /// </summary>
    public class GameManager : IGameManager
    {
        private readonly IFieldResolver _fieldResolver;
        private readonly IGameStatusResolver _gameStatusResolver;
        private readonly IMarkHelper _markHelper;
        private readonly IPlayerManager _playerManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameManager"/> class.
        /// </summary>
        public GameManager()
        {
            Board = new Board(this);

            _markHelper = new MarkHelper();
            _playerManager = new PlayerManager();
            _fieldResolver = new FieldResolver(_playerManager);
            _gameStatusResolver = new GameStatusResolver(_playerManager);
        }

        /// <summary>
        /// Gets the board manager used by this game manager.
        /// </summary>
        public Board Board { get; }

        /// <summary>
        /// Asynchronously performs player move upon specified field view model.
        /// </summary>
        /// <param name="sender">
        /// The related field view model.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Task"/> representing operation.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="field"/> is <see langword="null"/>.
        /// </exception>
        public async Task PerformPlayerMoveAsync(FieldVM field)
        {
            if (field == null)
            {
                throw new ArgumentNullException(nameof(field));
            }

            SetIsEnabledGlobally(isEnabled: false);
            MarkField(field, _playerManager.Human.Mark);

            // Check if field marked by player ends the game.
            var gameStatus = _gameStatusResolver.Resolve(Board.Fields);
            if (gameStatus.IsGameOver)
            {
                HandleGameEnding(gameStatus);
            }
            else
            {
                await PerformComputerMoveAsync();
                SetIsEnabledGlobally(isEnabled: true);
            }
        }

        private void HandleGameEnding(GameStatus gameStatus)
        {
            ShowPatternFieldsAnimation(gameStatus.WinningFields);

            var messageBoxTitle = gameStatus.EndingType == GameEndingType.Victory
                ? $"{gameStatus.Winner.Name} won!"
                : $"{gameStatus.EndingType}";

            var messageBoxResut = ShowEndOfGameMessageBox(messageBoxTitle);
            if (messageBoxResut == MessageBoxResult.Yes)
            {
                ResetFields();
                SetIsEnabledGlobally(isEnabled: true);
            }
            else
            {
                Environment.Exit(exitCode: 0);
            }
        }

        private void MarkField(FieldVM field, Mark mark)
        {
            if (field == null)
            {
                throw new ArgumentNullException(nameof(field));
            }

            field.Mark = mark;
            field.Content = field.Mark.Content;
            field.Foreground = _markHelper.GetMarkForeground(field.Mark);
            field.IsEnabled = _markHelper.GetMarkIsEnabled(field.Mark);
        }

        private async Task PerformComputerMoveAsync()
        {
            var field = await _fieldResolver.ResolveAsync(Board.Fields, _playerManager.Computer);
            MarkField(field, _playerManager.Computer.Mark);

            // Check if field marked by computer ends the game.
            var gameStatus = _gameStatusResolver.Resolve(Board.Fields);
            if (gameStatus.IsGameOver)
            {
                HandleGameEnding(gameStatus);
            }
        }

        private void ResetFields()
        {
            foreach (var field in Board.Fields)
            {
                MarkField(field, Mark.Empty);
            }
        }

        private void SetIsEnabledGlobally(bool isEnabled)
        {
            foreach (var field in Board.Fields)
            {
                if (isEnabled)
                {
                    if (field.HasEmptyMark)
                    {
                        field.IsEnabled = isEnabled;
                    }
                }
                else
                {
                    field.IsEnabled = isEnabled;
                }
            }
        }

        private void ShowPatternFieldsAnimation(IReadOnlyList<FieldVM> fields)
        {
            foreach (var field in fields)
            {
                var currentFieldColor = ((SolidColorBrush)field.Foreground).Color;
                var foregroundAnimation = new ColorAnimation
                {
                    AutoReverse = true,
                    From = currentFieldColor,
                    To = Colors.Black,
                    EasingFunction = new SineEase { EasingMode = EasingMode.EaseInOut },
                    Duration = new Duration(TimeSpan.FromMilliseconds(300)),
                    RepeatBehavior = RepeatBehavior.Forever
                };

                field.Foreground.BeginAnimation(SolidColorBrush.ColorProperty, foregroundAnimation);
            }
        }

        private MessageBoxResult ShowEndOfGameMessageBox(string title)
        {
            return MessageBox.Show("Would you like to play again?", title, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }
    }
}
