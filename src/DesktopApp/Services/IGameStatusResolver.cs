﻿using System.Collections.Generic;
using TicTacToe.DesktopApp.Models.Statuses;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    public interface IGameStatusResolver
    {
        /// <summary>
        /// Resolves game status based on provided collection of fields.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="GameStatus"/>.
        /// </returns>
        GameStatus Resolve(IEnumerable<FieldVM> fields);
    }
}
