﻿using System.Collections.Generic;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Represents minimum requirements for winning patterns repository.
    /// </summary>
    public interface IWinningPatternsRepository
    {
        /// <summary>
        /// Gets the winning patterns.
        /// </summary>
        ISet<WinningPattern> WinningPatterns { get; }
    }
}
