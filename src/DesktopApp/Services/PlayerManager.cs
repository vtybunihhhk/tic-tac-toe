﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Manages the players.
    /// </summary>
    [DebuggerDisplay("Count = {Players.Count}")]
    public class PlayerManager : IPlayerManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerManager"/> class.
        /// </summary>
        public PlayerManager()
        {
            Players = new List<Player>
            {
                Player.Computer,
                Player.You,
            };
        }

        /// <summary>
        /// Gets the computer player.
        /// </summary>
        public Player Computer => Players.FirstOrDefault(p => p.IsComputer);

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current player
        /// list containt at least one computer player.
        /// </summary>
        public bool HasComputerPlayer => Players.Any(p => p.IsComputer);

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current player
        /// list containt at least one human player.
        /// </summary>
        public bool HasHumanPlayer => Players.Any(p => p.IsHuman);

        /// <summary>
        /// Gets the human player.
        /// </summary>
        public Player Human => Players.FirstOrDefault(p => p.IsHuman);

        /// <summary>
        /// Gets the players collection.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Player> Players { get; }

        /// <summary>
        /// Returns the opposing player.
        /// </summary>
        /// <param name="player">
        /// The current player.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Player"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="player"/> is <see langword="null"/>.
        /// </exception>
        public Player GetOpponent(Player player)
        {
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            return Players.FirstOrDefault(p => p != player);
        }
    }
}
