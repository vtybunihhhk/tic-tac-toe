﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Defines minimum requirements for field resolver service.
    /// </summary>
    public interface IFieldResolver
    {
        /// <summary>
        /// Asynchronously finds next field that shoule be marked.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="Task{TResult}"/> representing operation.
        /// </returns>
        Task<FieldVM> ResolveAsync(IReadOnlyList<FieldVM> fields, Player player);
    }
}
