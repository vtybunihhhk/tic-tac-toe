﻿using System;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.Models.Statuses;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    ///
    /// </summary>
    public class GameStatusEvaluator : IGameStatusEvaluator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameStatusEvaluator"/> class.
        /// </summary>
        public GameStatusEvaluator()
        {

        }

        /// <summary>
        /// Returns calculated games status value.
        /// </summary>
        /// <param name="gameStatus">
        /// The game status to evaluate.
        /// </param>
        /// <param name="currentPlayer">
        /// The player holding the current turn torwards whom
        /// the evaluation should be made.
        /// </param>
        /// <returns>
        /// Game status value.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="currentPlayer"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="currentPlayer"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// Game is still ongoing.
        /// </exception>
        public int EvaluateGameStatus(GameStatus gameStatus, Player currentPlayer)
        {
            if (currentPlayer == null)
            {
                throw new ArgumentNullException(nameof(currentPlayer));
            }

            if (gameStatus == null)
            {
                throw new ArgumentNullException(nameof(gameStatus));
            }

            if (gameStatus.IsOngoing)
            {
                throw new InvalidOperationException(
                    "A field cannot be evaluated when game is still ongoing.");
            }

            if (gameStatus.IsTie)
            {
                return 0;
            }
            else
            {
                if (gameStatus.Winner.IsHuman)
                {
                    return -10;
                }
                else
                {
                    return 10;
                }
            }
        }
    }
}
