﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Determines minimum requiremenets for game manager service.
    /// </summary>
    public interface IGameManager
    {
        /// <summary>
        /// Asynchronously performs player move upon specified field view model.
        /// </summary>
        /// <param name="sender">
        /// The related field view model.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Task"/> representing operation.
        /// </returns>
        Task PerformPlayerMoveAsync(FieldVM field);
    }
}
